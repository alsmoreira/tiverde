/*
 ============================================================================
 Name          : main.cpp
 Author        : Anderson Moreira
 E-mail        : anderson.moreira@recife.ifpe.edu.br
 Version       : 05 de dez de 2023
 Copyright     :
 Description   : O código a seguir retorna o valor do clock do sistema para
                    referência em sistemas de auditoria. Está compilado no
                    CPP-17 POSIX.
 ============================================================================
 */


#include <thread>
#include <chrono>
#include <cstdio>
#include <iostream>

//Usa o comando assembly para obter o valor atual do "contador" de ciclos
uint64_t get_cycles()
{
    unsigned int lo, hi;
    __asm__ __volatile__("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

//Define a unidade de tempo para medir os segundos
typedef std::chrono::duration<double, std::ratio<1, 1>> seconds_t;

//Em main(int argc, char** argv) esta variável global é inicializada para argv[0]
const char* program_name;

//Inicializado no início do programa; obtém a hora atual
auto start_time = std::chrono::high_resolution_clock::now();

//Retorna o tempo desde o início do programa, medido em segundos.
// Tem precisão identica de std::chrono::high_resolution_clock
double age()
{
    return seconds_t(std::chrono::high_resolution_clock::now() - start_time).count();
}

//Imprime o uso do programa
void PrintUsage()
{
    printf("Uso:\n");
    printf("%s [medida duracao]\n", program_name);
}

int main(int argc, char** argv)
{
    using namespace std::chrono_literals;
    program_name = argv[0];
    int sleeptime = 100;
    switch (argc) {
        case 1:
            sleeptime = 100;
            break;
        case 2:
            try
            {
                sleeptime = std::stoi(argv[1]);
            }
            catch (...)
            {
                printf("Erro: o argumento nao e um inteiro.");
                PrintUsage();
                return 1;
            }
            break;
        default:
            printf("Erro: quantidade de parametros\n");
            PrintUsage();
            return 1;
            break;
    }
    uint64_t cycles_start = get_cycles();
    double time_start = age();
    std::this_thread::sleep_for(sleeptime * 1ms);
    uint64_t elapsed_cycles = get_cycles() - cycles_start;
    double elapsed_time = age() - time_start;
    printf("CPU MHz: %.3f\n", elapsed_cycles / elapsed_time / 1000000.0);
}