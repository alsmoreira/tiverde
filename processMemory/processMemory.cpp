
/*
 ============================================================================
 Name          : processMemory.cpp
 Author        : Anderson Moreira
 E-mail        : anderson.moreira@recife.ifpe.edu.br
 Version       : 27 de nov de 2023
 Copyright     :
 Description   : Para determinar a efici�ncia do aplicativo, conv�m 
                    examinar seu uso de mem�ria. O c�digo usa a fun��o
                    GetProcessMemoryInfo para obter informa��es sobre 
                    o uso de mem�ria de um processo.
                    A fun��o principal obt�m uma lista de processos 
                    usando a fun��o EnumProcesses. Para cada processo, 
                    main chama a fun��o PrintMemoryInfo, passando 
                    o identificador do processo. PrintMemoryInfo, por sua vez, 
                    chama a fun��o OpenProcess para obter o identificador 
                    de processo. Se o OpenProcess falhar, a sa�da 
                    mostrar� apenas o identificador do processo. 
                    Por exemplo, o OpenProcess falha para os processos 
                    Idle e CSRSS porque suas restri��es de acesso impedem 
                    que o c�digo no n�vel do usu�rio os abra. 
                    Finalmente, PrintMemoryInfo chama a fun��o 
                    GetProcessMemoryInfo para obter as informa��es de uso 
                    de mem�ria.
                    Op��o -DPSAPI_VERSION=1
 ============================================================================
 */

#include <windows.h>
#include <stdio.h>
#include <psapi.h>


void PrintMemoryInfo(DWORD processID)
{
     HANDLE hProcess;
     PROCESS_MEMORY_COUNTERS pmc;

     // Mostra o identificador do processo

     printf("\nProcess ID: %u\n", processID);

     // Imprima informa��es sobre o uso de mem�ria do processo.

     hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
          PROCESS_VM_READ,
          FALSE, processID);
     if (NULL == hProcess)
          return;

     if (GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc)))
     {
          printf("\tPageFaultCount: 0x%08X\n", pmc.PageFaultCount);
          printf("\tPeakWorkingSetSize: 0x%08X\n", pmc.PeakWorkingSetSize);
          printf("\tWorkingSetSize: 0x%08X\n", pmc.WorkingSetSize);
          printf("\tQuotaPeakPagedPoolUsage: 0x%08X\n", pmc.QuotaPeakPagedPoolUsage);
          printf("\tQuotaPagedPoolUsage: 0x%08X\n", pmc.QuotaPagedPoolUsage);
          printf("\tQuotaPeakNonPagedPoolUsage: 0x%08X\n", pmc.QuotaPeakNonPagedPoolUsage);
          printf("\tQuotaNonPagedPoolUsage: 0x%08X\n", pmc.QuotaNonPagedPoolUsage);
          printf("\tPagefileUsage: 0x%08X\n", pmc.PagefileUsage);
          printf("\tPeakPagefileUsage: 0x%08X\n", pmc.PeakPagefileUsage);
     }

     CloseHandle(hProcess);
}

int main(void)
{
     // Obtenha a lista de identificadores de processo.

     DWORD aProcesses[1024], cbNeeded, cProcesses;
     unsigned int i;

     if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
     {
          return 1;
     }

     // Calcule quantos identificadores de processo foram retornados.

     cProcesses = cbNeeded / sizeof(DWORD);

     // Imprimir o uso de mem�ria para cada processo

     for (i = 0; i < cProcesses; i++)
     {
          PrintMemoryInfo(aProcesses[i]);
     }


     return 0;
}