/*
 * info.h
 *
 *  Created on: 05/12/2023
 *      Author: Anderson Moreira
 *      e-mail: anderson.moreira@recife.ifpe.edu.br
 *
 *  O rel�gio de um PC � atualizado em torno de 18 vezes por segundo com uma resolu��o de
 *  0,05 a 0,06 segundos que � similar ao tempo pego pela fun��o principal de consumo do
 *  tempo 'dgefa' em um Pentium 100MHz. Esta rotina trabalha com ponto flutuante de 64 bits.
 *  Ele cont�m dois conjuntos de rotinas: um para decomposi��o de matrizes e outro para resolver
 *  o sistema de equa��es lineares resultantes da decomposi��o. Os resultados dos testes s�o
 *  reportados em MFLOPS, GFLOPS ou TFLOPS. Sua aplica��o � visivelmente em m�quinas que utilizam 
 *  softwares para c�lculos cient�ficos e de engenharia, visto que as opera��es mais utilizadas 
 *  nestes tipos de aplica��es s�o em ponto-flutuante.
 */

#define NTIMES 10

#define REAL double
#define ZERO 0.0e0
#define ONE 1.0e0
#define PREC "Double "
#define ROLLING "Rolled "

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stddef.h>
#include <time.h>  /* para fun��o de tempo */

/*Inicio de bibliotecas para identifica��o de hardware */
#include <windows.h>
#include <cstdio>
#include <thread>
#include <chrono>
#include <psapi.h>
#include <tchar.h>

#define KEY_PROCESSADOR "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0"
#define KEY_VIDEO "HARDWARE\\DEVICEMAP\\VIDEO"

static REAL atime[9][15];

#ifndef INFO_H_
#define INFO_H_


#endif /* INFO_H_ */
