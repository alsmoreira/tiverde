/*
 ============================================================================
 Name          : getmemory.cpp
 Author        : Anderson Moreira
 E-mail        : anderson.moreira@recife.ifpe.edu.br
 Version       : 22 de nov de 2023
 Copyright     :
 Description   : Uso da fun��o GlobalMemoryStatusEx para determinar quanta 
               mem�ria seu aplicativo pode alocar sem afetar gravemente 
               outros aplicativos. As informa��es retornadas pela fun��o 
               GlobalMemoryStatusEx s�o vol�teis. N�o h� garantia de que 
               duas chamadas sequenciais para essa fun��o retornar�o as 
               mesmas informa��es.
 ============================================================================
 */

#include "info.h"

// converter bytes em KB
constexpr auto DIV = 1024;

// Especifique a largura do campo no qual quer imprimir os n�meros. 
// O asterisco no especificador de formato "%*I64d" usa um argumento 
// inteiro e o usa para preencher e justificar o n�mero.
// TO-DO melhorar sa�da
#define WIDTH 7

REAL second()
{
     REAL secs;
     clock_t Time;
     Time = clock();
     secs = (REAL)Time / (REAL)CLOCKS_PER_SEC;
     return secs;
}

int main()
{
     MEMORYSTATUSEX statex{};

     statex.dwLength = sizeof(statex);

     GlobalMemoryStatusEx(&statex);

     _tprintf(TEXT("Existe  %*ld porcento de memoria em uso.\n"), WIDTH, statex.dwMemoryLoad);
     _tprintf(TEXT("Existe %*I64d KB total de memoria fisica.\n"), WIDTH, statex.ullTotalPhys / DIV);
     _tprintf(TEXT("Existe %*I64d KB livre de memoria fisica.\n"), WIDTH, statex.ullAvailPhys / DIV);
     _tprintf(TEXT("Existe %*I64d KB total de arquivo de paginacao.\n"), WIDTH, statex.ullTotalPageFile / DIV);
     _tprintf(TEXT("Existe %*I64d KB livre de arquivo de paginacao.\n"), WIDTH, statex.ullAvailPageFile / DIV);
     _tprintf(TEXT("Existe %*I64d KB total de memoria virtual.\n"), WIDTH, statex.ullTotalVirtual / DIV);
     _tprintf(TEXT("Existe %*I64d KB livre de memoria virtual.\n"), WIDTH, statex.ullAvailVirtual / DIV);

     // Mostrar a quantidade de mem�ria estendida dispon�vel.

     _tprintf(TEXT("Existe %*I64d KB livre de memoia estendida.\n"), WIDTH, statex.ullAvailExtendedVirtual / DIV);

     return 0;
}