/*
 ============================================================================
 Name          : getDevice.cpp
 Author        : Anderson Moreira
 E-mail        : anderson.moreira@recife.ifpe.edu.br
 Version       : 27 de nov de 2023
 Copyright     :
 Description   : O código de exemplo a seguir usa a função EnumDeviceDrivers 
                    para enumerar os drivers de dispositivo atuais no 
                    sistema. Ele passa os endereços de carga recuperados 
                    dessa chamada de função para a função GetDeviceDriverBaseName 
                    para recuperar um nome que pode ser exibido.
 ============================================================================
 */

#include <windows.h>
#include <psapi.h>
#include <tchar.h>
#include <stdio.h>

 // Compilar com -DPSAPI_VERSION=1

#define ARRAY_SIZE 1024

int main(void)
{
     LPVOID drivers[ARRAY_SIZE];
     DWORD cbNeeded;
     int cDrivers, i;

     if (EnumDeviceDrivers(drivers, sizeof(drivers), &cbNeeded) && cbNeeded < sizeof(drivers))
     {
          TCHAR szDriver[ARRAY_SIZE];

          cDrivers = cbNeeded / sizeof(drivers[0]);

          _tprintf(TEXT("O sistema tem %d dispositivos em memoria:\n"), cDrivers);
          for (i = 0; i < cDrivers; i++)
          {
               if (GetDeviceDriverBaseName(drivers[i], szDriver, sizeof(szDriver) / sizeof(szDriver[0])))
               {
                    _tprintf(TEXT("%d: %s\n"), i + 1, szDriver);
               }
          }
     }
     else
     {
          _tprintf(TEXT("EnumDeviceDrivers failed; array size needed is %d\n"), cbNeeded / sizeof(LPVOID));
          return 1;
     }

     return 0;
}
